###Отчет по лабораторной работе №2

*Я решил сделать отчет по пунктам*

1. **Аналоги**.
    - Среди всех предложенных аналогов мне больше всего приглянулись **Wunderlist** и **Яндекс.Трекер**. У первого по моему мнению интуитивно понятнее дизайн, у второго достаточно обширный функционал, но мне показался Яндекс.Трекер более сложным для *рядового* пользователя.    
    - Я раньше использовал **Google Keep** для каких-то своих заметок, составления списка дел. На мой взгляд данная программа отлично подходит для заметок (напоминания, важные заметки и т.д.), но как трекер дел она не годится. Так же использовал программу **Заметки от компании Xiaomi**. Но обе этих программы подходят разве что для походов в магазин, небольших каких-то заметок, напоминаний о каких-то делах.    
    - В **Wunderlist** мне понравилось создание **категорий**(*работа, дом, проект ИСП*), после создания категории ты в этой категории можешь создать **задачи**, в задачах можешь создавать **подзадачи** и присваивать их другим пользователям программы (*в платной версии*). Так же можно добавить **дедлайн** для задачи. После выполнения всех подзадач, задача считается выполненной и скидывается в выполненные задачи. Задачи можно помечать **пометкой важное**.    
2. **Cценарии использования**.
    - **Первый** сценарий использования **повседневная семейная жизнь**. Создается категория семья, в это категории создаются определенные задачи, ставятся сроки выполнения этих задач, назначаются пользователи ответственные за выполнение, по мере необходимости добавляются подзадачи.  
    - **Второй** сценарий **работа**. К примеру группа людей получила задание *написать сайт*. Создается категория *сайт*, добавляются задачи: *фронт-энд, бэк-энд, тестирование*. Назначаются на задачи ответсвенные люди, сроки сдачи, приоритет. Потом в каждую задачу добавляются подзадачи.  
    - **Третий** сценарий - **личное время**. Здесь можно придумать много примеров, самый простой из них - чтение книг. Пользователь решил за год прочитать 50 книг. Создает Категории книги, добавляет книги, ставит дедлайны и т.д.  
    - **Четвертый** сценарий - **распорядок дня**. У пользователя есть определенный список дел, которые нужно реализовать в определенные день, он добавляет их, планирует свой день, добавляет напоминания и т.д. В этой категории так же много примеров использования, все зависит от потребностей пользователя.  
    - **Пятый** сценарий - **учеба**. Например, группе студентов дали подготовить большой реферать на обширную тему. Они создают категорию реферат, разделяют большую тему на более мелкие, каждому назначаются определенные подтемы, ставятся сроки выполнения на эти подтемы. Так же выставляются приоритеты подтем, к примеру можно опустить менее важные подтемы, если будет поджимать время. Можно будет следить за ходом проекта.
    - **Шестой** сценарий - **повторяющиеся дела**. Данный тип дел был описан в файле лабораторной работы, я его опишу так, как сам вижу. К примеру у меня по расписанию каждый вторник, четверг бассейн. Я создаю себе задачу, чтобы каждый понедельник и среду вечером мне показывалось напоминание о том, что необходимо собрать вещи в бассейн. Так же в файле лабороторной работы был пример с лекарствами.
3. **Планируемые возможности**.
    - Добавление/выбор пользователя.
    - Редактирование профиля пользователя.
    - Добавление категорий.
    - Добавление задач в категорию.
    - Добавление подзадач в задачу.
    - Выставление сроков для задачи.
    - Присваивание задачам приоритеты.
    - Присваивать задачи другим пользователям.
    - Добавлять комментарии к задачам.
    - Установка напоминаний.
    - Отображение процента выполненных задач в категории(*например, у нас есть Категория, в ней 10 задач, из этих задач выполненой 5 -> 50% категории выполнены*).
    - Отображение процента выполненных подзадач в задаче(*аналогично отображению процента выполненных задач в категории*)
    - Автоматически обозначать задачу завершенной, если все подзадачи выполнены(*при наличии подзадач у задачи*)
    - Возможность создавать *повторяющиеся дела*.
    - Возможность открывать календарь и видеть сроки для задач, подзадач(*как в Google Calendar*).
    - Возможность отправки напоминаний на почту пользователя.
4. **Логическая архитектура**.
    - Имеется **основное окно программы**, где отображается имя пользователя, категории задач, кнопка для добавления категории, поле для поиска задачи во всех категориях, кнопка для открытия календаря.
    - Окно **Добавление категории** позволяет ввести название новой категории, выбрать участников категории и создать новую категорию.
    - При нажатии на название категории у нас открывается собственно эта категория (как в файловом менеджере).
    - В категории у нас списком выводятся задачи, есть кнопка для добавления задачи, кнопка параметров сортировки задач, кнопка для добавления пользователя к категории.
    - При нажатии на кнопку **добавление задачи** открывается окно, где пользователю предлагается ввести название задачи, задать напоминания, задать крайний срок, добавить подзадачи, добавить комментарий, выставить приоритет.
    - При добавлении подзадач они отображаются в виде списка, где можно отмечать галочкой уже выполненные подзадачи(*Как в Wunderlist*).
    - После создания задачи она у нас отображается в категории.
    - При нажатии на задачу в категории открывается окно редактирования задачи, где можно поменять крайний срок, добавить подзадачу, добавить комментарий.
    - Для того, чтобы отметить, что задачи выполнена, достаточно просто пометить её галочкой.
    - При нажатии на кнопку **добавить пользователя** открывается окно добавления пользователей, где есть поле для ввода имени/адреса эл. почты пользователя, а так же список уже добавленных пользователей в данную категорию.
    - **Кнопка для открытия календаря** открывает календарь, где показываются сроки для задач (*как в Google Calendar*).
5. **Этапы разработки**
    - Спроектировать приложение, написать файл *STATUS.md*
    - В базовой версии я планирую реализовать возможность добавления категорий/задач/подзадач, редактирование задач.
    - В расширенной версии возможность работы с пользователями(*создание, редактирование*), присваивать задачи пользователям, добавить шаблон для создания повторяющихся дел, добавить возможность комментировать задачи.
    - В веб-версии создать графический интерфейс, добавить календарь, где будут отображаться сроки для задач, добавить возможность отправки напоминаний на почту пользователя.
6. **Сроки**
    *Ориентировочные сроки написания/сдачи версий*    
    - Базовая версия: 31.03 - 05.04
    - Расширенная версия: 25.04-30.04
    - Веб-версия: 22.05-30.05
7. **Статус**
    - Написать файл *Status.md*
    - Реализовать базовую версию (*добавление категорий/задач/подзадач, редактирование задач*)
    - Реализовать расширенную версию (*работа с пользователями, присваивание задач, шаблоны задач, возможность комментировать задачи*)
    - Реализовать веб-версию (*создать графический интерфейс, добавить календарь, добавить возможность отправки напоминаний на почту пользователя*)
